import java.io.*;
import java.net.URI;
import java.net.URL;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class xmlReader {

 public String getElementValue(String filename, String strElemToFind) throws Exception {
  // Pre: filename is the name of the file to be parsed for xml .strElimTofind contains the xml element to find in the file.
  //Post returns either the first value that is found ifor thestrElemToFind or null if strElemToFind does not exist.
  
	 File file = new File(filename);
	
  if (!file.exists()) {
   throw new Exception("Configuration file not found: " + filename);
  }
  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  Document doc = factory.newDocumentBuilder().parse(file);
  NodeList nodelist = doc.getElementsByTagName(strElemToFind);
  if (nodelist.getLength() < 1) {
   //throw new Exception("No " + strElemToFind +" element not found in configuration file")
  }
  //System.out.println("There are " + nodelist.getLength() + "nodes.");
  return (nodelist.item(0).getChildNodes().item(0).getNodeValue());
  /*
  The following section is for testing how to displauy all named nodes in the document.


  for (int i = 0; i < nodelist.getLength(); i++); {
    Node addressNode = nodelist.item(i);
    Element address = (Element)addressNode;
    //Create an element by casting the node
    NodeList data = address.getChildNodes();
    //Create a nodelist from the childfresn of the element
    System.out.println(data.item(0).getNodeValue());

    //The following line deonstrates how to get the value without creating the 'data' NodeList
    //System.out.println(address.getChildNode().item(0).getNodeValue());

  }
 */
 }

 public String getCodePath() {
  String strTemp = this.getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
  return strTemp;
 }

 public static void main(String[] args) {
  try {
   xmlReader xReader = new xmlReader();
   System.out.println(xReader.getElementValue("config.xml", "IPAddress"));
   Class testClass = xReader.getClass();
   System.out.println(xReader.getCodePath());
  } catch (Exception e) {
   System.out.println("Exception encountered: " + e.getMessage());
  }
 }
}
